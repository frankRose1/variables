"""
  main.py - main file for the app
  Contains variables about the different attributes in a song
  and prints the variables to the console
"""

#variable declarations START
song_title = 'Enter Sandman'
year_released = 1991
month_released = 'July'
artist = 'Metallica'
genre = 'Heavy Metal'
duration_minutes = '5:32'
duration_seconds = 319.2
instrument = 'guitar'
song_writers = 'James Hetfield, Lars Ulrich, Kirk Hammett'
classic = True
recording_location = 'One on One Studios, Los Angeles'
#variable declarations END


#printing variables START
print(song_title)
print(year_released)
print(month_released)
print(artist)
print(genre)
print(duration_minutes)
print(duration_seconds)
print(instrument)
print(song_writers)
print(classic)
print(recording_location)
#printing variables END