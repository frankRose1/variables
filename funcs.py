"""
  File contains functions that return the corresponding
  value of the variables found in main.py
"""


def artist():
    artist_name = 'Metallica'
    return artist_name


def genre():
    music_genre = 'Heavy Metal'
    return music_genre


def duration_seconds():
    duration = 319.2
    return duration


def is_release_date(month):
    if month == 'July':
        return True
    else:
        return False


print(artist())
print(genre())
print(duration_seconds())
print(is_release_date('August'))  # False
print(is_release_date('July'))  # True
